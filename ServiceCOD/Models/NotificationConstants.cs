﻿using System;



public class NotificationConstants
{
    public const string RoleNotificationFile = "roleNotifications.json";
    public const string ProductNotificationFile = "productNotifications.json";
}
